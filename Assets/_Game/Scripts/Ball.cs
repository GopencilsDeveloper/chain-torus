﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class Ball : MonoBehaviour
{
    #region Editor Params
    public Collider collider;
    #endregion

    #region Params
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnDisable()
    {
        gameObject.layer = LayerMask.NameToLayer("Default");
        collider.isTrigger = true;

        if (GetComponent<Rigidbody>() != null)
        {
            Destroy(GetComponent<Rigidbody>());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Torus"))
        {
            collider.isTrigger = false;
            Rigidbody rgbd = gameObject.GetComponent<Rigidbody>();
            if (rgbd == null)
            {
                rgbd = gameObject.AddComponent<Rigidbody>();
                rgbd.mass = 5f;
            }
            rgbd.mass = 5f;
            rgbd.useGravity = true;
            MMVibrationManager.VibrateLight();
            transform.SetParent(transform.root);
        }

        if (other.gameObject.CompareTag("TopHole"))
        {
            gameObject.layer = LayerMask.NameToLayer("OnHole");
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            ScoreFX scoreFX = ScoreFXPool.Instance.GetFromPool();
            scoreFX.transform.position = transform.position;
            scoreFX.SetScore(1);
            scoreFX.DoAnim();

            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            UIManager.Instance.UpdateScore(1);
        }

        if (other.gameObject.CompareTag("BotHole"))
        {
            ScoreFX scoreFX = ScoreFXPool.Instance.GetFromPool();
            scoreFX.transform.position = transform.position;
            scoreFX.SetScore(2);
            scoreFX.DoAnim();

            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            UIManager.Instance.UpdateScore(1);
        }

    }
    #endregion
}
