﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallRing : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    List<Ball> listBalls = new List<Ball>();
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnEnable()
    {
        SpawnBalls(8);
    }

    public void SpawnBalls(int count = 4)
    {
        listBalls.Clear();
        float angleStep = 360f / count;
        for (int i = 0; i < count; i++)
        {
            Ball ball = BallPool.Instance.GetFromPool();
            ball.transform.SetParent(transform);
            ball.transform.localPosition = Vector3.zero;
            ball.transform.localRotation = Quaternion.Euler(0f, 0f, angleStep * i);
            listBalls.Add(ball);
        }
    }

    public void ReclaimBalls()
    {
        foreach (Ball ball in listBalls)
        {
            BallPool.Instance.ReturnToPool(ball);
        }
        listBalls.Clear();
    }

    #endregion
}
