﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFOV : MonoBehaviour
{
    private void Start()
    {
        UpdateFOV();
    }

    public void UpdateFOV()
    {
        float screenRatio = Camera.main.aspect;
        if (screenRatio <= 0.475f)  //9:19
        {
            Camera.main.fieldOfView = 60f;
        }
        else
        if (screenRatio <= 0.565f)  //9:16
        {
            Camera.main.fieldOfView = 55f;
        }
        else
        if (screenRatio <= 0.76)  //3:4
        {
            Camera.main.fieldOfView = 45f;
        }
    }
}
