﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomPivot : MonoBehaviour
{
    private void SetParentPivot(Transform pivot)
    {
        Transform pivotParent = pivot.parent;

        MeshFilter meshFilter = pivotParent.GetComponent<MeshFilter>();
        Mesh originalMesh = null;
        if (!IsNull(meshFilter) && !IsNull(meshFilter.sharedMesh))
        {
            originalMesh = meshFilter.sharedMesh;
            Mesh mesh = Instantiate(meshFilter.sharedMesh);
            meshFilter.sharedMesh = mesh;

            Vector3[] vertices = mesh.vertices;
            Vector3[] normals = mesh.normals;
            Vector4[] tangents = mesh.tangents;

            if (pivot.localPosition != Vector3.zero)
            {
                Vector3 deltaPosition = -pivot.localPosition;
                for (int i = 0; i < vertices.Length; i++)
                    vertices[i] += deltaPosition;
            }

            if (pivot.localEulerAngles != Vector3.zero)
            {
                Quaternion deltaRotation = Quaternion.Inverse(pivot.localRotation);
                for (int i = 0; i < vertices.Length; i++)
                {
                    vertices[i] = deltaRotation * vertices[i];
                    normals[i] = deltaRotation * normals[i];

                    Vector3 tangentDir = deltaRotation * tangents[i];
                    tangents[i] = new Vector4(tangentDir.x, tangentDir.y, tangentDir.z, tangents[i].w);
                }
            }

            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.tangents = tangents;

            mesh.RecalculateBounds();
        }


        Collider[] colliders = pivotParent.GetComponents<Collider>();
        foreach (Collider collider in colliders)
        {
            MeshCollider meshCollider = collider as MeshCollider;
            if (!IsNull(meshCollider) && !IsNull(originalMesh) && meshCollider.sharedMesh == originalMesh)
            {
                meshCollider.sharedMesh = meshFilter.sharedMesh;
            }
        }

        Transform[] children = new Transform[pivotParent.childCount];
        Vector3[] childrenLocalScales = new Vector3[children.Length];
        for (int i = children.Length - 1; i >= 0; i--)
        {
            children[i] = pivotParent.GetChild(i);
            childrenLocalScales[i] = children[i].localScale;

            children[i].SetParent(null, true);
        }

        pivotParent.position = pivot.position;
        pivotParent.rotation = pivot.rotation;

        for (int i = 0; i < children.Length; i++)
        {
            children[i].SetParent(pivotParent, true);
            children[i].localScale = childrenLocalScales[i];
        }

        pivot.localPosition = Vector3.zero;
        pivot.localRotation = Quaternion.identity;
    }

    private bool IsNull(Object obj)
    {
        return obj == null || obj.Equals(null);
    }
}
