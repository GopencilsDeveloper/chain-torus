﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    #region CONST
    public const string LEVEL = "LEVEL";
    public const string SCORE = "SCORE";
    #endregion

    #region Editor Params
    #endregion

    #region Params
    public int currentLevel;
    public int currentScore;
    #endregion

    #region Properties
    public static DataManager Instance { get; private set; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        Instance = this;
        LoadData();
        // ResetData();
        // PlayerPrefs.SetInt(LEVEL, 20);
        // currentLevel = PlayerPrefs.GetInt(LEVEL);
    }

    public void LoadData()
    {
        currentLevel = PlayerPrefs.GetInt(LEVEL);
        currentScore = PlayerPrefs.GetInt(SCORE);
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt(LEVEL, currentLevel);
        PlayerPrefs.SetInt(SCORE, currentScore);
    }

    public void ResetData()
    {
        PlayerPrefs.SetInt(LEVEL, 0);
        PlayerPrefs.SetInt(SCORE, 0);
        SaveData();
    }
    #endregion
}
