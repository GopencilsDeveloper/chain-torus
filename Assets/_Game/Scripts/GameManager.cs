﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    NULL, MENU, INGAME, WINGAME, LOSEGAME
}

public class GameManager : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    public static GameManager Instance { get; private set; }
    public GameState currentState;
    private bool isFirstStart;
    #endregion

    #region Properties
    #endregion

    #region Events
    public static event System.Action<GameState> OnStateChanged;
    #endregion

    #region Methods

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        ChangeState(GameState.MENU);
        LevelManager.Instance.SpawnLevel();
        ThemeManager.Instance.RandomTheme();
        isFirstStart = true;
    }

    public void RestartGame()
    {
        ChangeState(GameState.INGAME);
        if (isFirstStart)
        {
            isFirstStart = false;
            return;
        }
        LevelManager.Instance.SpawnLevel();
        ThemeManager.Instance.RandomTheme();
    }

    public void WinGame()
    {
        ChangeState(GameState.WINGAME);
        DataManager.Instance.currentLevel++;
        DataManager.Instance.SaveData();
    }

    public void LoseGame()
    {
        ChangeState(GameState.LOSEGAME);
        DataManager.Instance.SaveData();
    }

    public void ChangeState(GameState state)
    {
        currentState = state;
        if (OnStateChanged != null)
        {
            OnStateChanged(state);
        }
    }

    #endregion
}
