﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GeneratePipe : MonoBehaviour
{
    [Header("Properties")]
    public GameObject spherePrefab;
    public GameObject block;
    public GameObject torusPrefab;
    public BallRing ballRingPrefab;
    public Transform ballRingsStorage;
    public Transform pipesStorage;
    public Transform predict;
    public Transform centerObj;
    public float radius;
    public int count;
    public float length;
    private bool isGenerating;
    private float percentLength;

    private List<Vector3> paths = new List<Vector3>();
    private List<GameObject> listSpheres = new List<GameObject>();
    private List<GameObject> listRings = new List<GameObject>();
    private List<BallRing> listBallRings = new List<BallRing>();


    private Vector3 centerPos;
    private LayerMask groundLayerMask;
    public LineDrawer lineDrawer;

    [Header("Save Editor")]
    public string FileName;
    private bool isSave;

    private void Start()
    {
        groundLayerMask = 1 << 9;
    }

    private void Update()
    {
        if (lastSphere != null && preLastSphere != null)
        {
            RaycastHit hit;
            lastDirectionForRay = (lastSphere.position - preLastSphere.position).normalized;

            if (Physics.Raycast(lastSphere.position, lastDirectionForRay * 1000f, out hit, Mathf.Infinity, groundLayerMask))
            {
                if (!predict.gameObject.activeInHierarchy)
                {
                    predict.gameObject.SetActive(true);
                }
                predict.position = hit.point;
                // lineDrawer.DrawLineInGameView(lastSphere.position, hit.point, Color.green);
            }
            else
            {
                if (predict.gameObject.activeInHierarchy)
                {
                    predict.gameObject.SetActive(false);
                }
            }
        }
    }

    public void GeneratePaths()
    {
        if (isGenerating || isSave) return;
        isGenerating = true;
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        paths.Clear();
        listSpheres.Clear();
        listRings.Clear();
        isGenRing = false;
        StartCoroutine(C_GeneratePaths());
    }

    private IEnumerator C_GeneratePaths()
    {
        for (int i = 0; i < count; i++)
        {
            if (i == 0)
            {
                Vector3 firstPosition = Vector3.zero;
                GenerateFirstSphere(firstPosition);
                paths.Add(firstPosition);
            }
            else
            {
                bool isLoop = true;
                int n = 0;

                while (isLoop)
                {
                    Vector3 point = PositionRandom() + paths[i - 1];

                    RaycastHit hit;
                    Vector3 direction = (point - paths[i - 1]).normalized;
                    float distance = Vector3.Distance(paths[i - 1], point) * 1.5f;

                    bool isCollisionPipe = Physics.SphereCast(paths[i - 1], 3, direction, out hit, distance);
                    bool isOutScreen = (Mathf.Abs(point.x) >= radius || Mathf.Abs(point.y) >= radius || Mathf.Abs(point.z) >= radius) ? true : false;
                    bool isSameDirection = (i < 2) ? false : IsSameDirection(paths[i - 2], paths[i - 1], point);


                    if (isCollisionPipe == false && isOutScreen == false && isSameDirection == false)
                    {
                        GenerateSphere(point, paths[i - 1]);
                        paths.Add(point);
                        isLoop = false;
                    }

                    n++;

                    if (n > 20)
                    {
                        Debug.LogError("Error: Can not find the way out.");
                        isGenerating = false;
                        SetCenter();
                        yield break;
                    }

                    yield return null;
                }
            }
            yield return null;
        }

        isGenerating = false;
        SetCenter();
    }

    private Vector3 PositionRandom()
    {
        if (length == 0) Debug.LogError("Error: length = 0");
        percentLength += 0.25f;

        if (percentLength > 1) percentLength = 0.5f;

        float lengthFixed = length * percentLength;

        float x = ValueRandom() * lengthFixed;
        float y = ValueRandom() * lengthFixed;
        float z = ValueRandom() * lengthFixed;

        int r = Random.Range(0, 3);

        if (r == 0)
        {
            x = 0.0f;
            y = 0.0f;
        }
        else if (r == 1)
        {
            x = 0.0f;
            z = 0.0f;
        }
        else
        {
            z = 0.0f;
            y = 0.0f;
        }

        return new Vector3(x, y, z);
    }

    private float ValueRandom()
    {
        return Random.value > 0.5f ? 1 : -1;
    }

    private bool IsSameDirection(Vector3 a, Vector3 b, Vector3 c)
    {
        Vector3 ab = b - a;
        Vector3 bc = c - b;

        bool isSamePosX = (ab.x == bc.x) ? true : false;
        bool isSamePosY = (ab.y == bc.y) ? true : false;
        bool isSamePosZ = (ab.z == bc.z) ? true : false;

        if (isSamePosX && isSamePosY)
        {
            return true;
        }
        else if (isSamePosX && isSamePosZ)
        {
            return true;
        }
        else if (isSamePosY && isSamePosZ)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool isGenRing;

    private void GenerateSphere(Vector3 pos, Vector3 prePos)
    {
        GameObject pipe = Instantiate(spherePrefab, transform) as GameObject;
        pipe.transform.position = pos;

        listSpheres.Add(pipe);

        Transform pipeObj = pipe.transform.GetChild(0);
        pipeObj.rotation = Quaternion.LookRotation(prePos - pos);

        if (!isGenRing)
        {
            GenerateRings(3, (pos + prePos) * 0.5f, (pos - prePos).normalized);
            GenerateBlock((pos - prePos).normalized);
            isGenRing = true;
        }

        float distance = Vector3.Distance(pos, prePos) / 2.0f;

        Vector3 pipeScale = pipeObj.localScale;
        pipeScale.z = distance;
        pipeObj.localScale = pipeScale;

        Transform ballRings = pipe.transform.GetChild(1);
        ballRings.rotation = pipeObj.rotation;
        int ballRingsCount = (int)(pipeScale.z * 2f);

        GenerateBallRings(ballRingsCount, (pos - prePos).normalized, ballRings);
        pipe.transform.SetParent(pipesStorage);
    }

    private void GenerateBallRings(int count, Vector3 direction, Transform parent)
    {
        for (int i = 0; i < count; i++)
        {
            BallRing ballRing = Instantiate(ballRingPrefab, parent);
            ballRing.transform.localPosition = Vector3.zero;
            ballRing.transform.localPosition += new Vector3(0f, 0f, 1f) * i;
            listBallRings.Add(ballRing);
            ballRing.transform.SetParent(ballRingsStorage);
        }
    }

    private void GenerateFirstSphere(Vector3 pos)
    {
        GameObject sphere = Instantiate(spherePrefab, transform) as GameObject;
        sphere.transform.position = pos;
        listSpheres.Add(sphere);
    }

    private void GenerateBlock(Vector3 direction)
    {
        // GameObject block = Instantiate(blockPrefab, transform) as GameObject;
        block.transform.position = Vector3.zero;
        if (direction.x != 0)
        {
            block.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
        }
        else
        if (direction.z != 0)
        {
            block.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        }
        else
        if (direction.y != 0)
        {
            block.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }

    private void GenerateRings(int count, Vector3 pos, Vector3 direction)
    {
        for (int i = 0; i < 3; i++)
        {
            GameObject torus = Instantiate(torusPrefab, transform);
            torus.transform.position = pos;
            listRings.Add(torus);
            if (direction.x != 0)
            {
                torus.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
                torus.transform.position += new Vector3(1f, 0f, 0f) * i;
            }
            else
            if (direction.z != 0)
            {
                torus.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
                torus.transform.position += new Vector3(0f, 0f, 1f) * i;
            }
            else
            if (direction.y != 0)
            {
                torus.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                torus.transform.position += new Vector3(0f, 1f, 0f) * i;
            }
        }
    }


    public void SaveToFile()
    {
        Debug.Log("Start export.");
        isSave = true;

        if (FileName == "")
        {
            isSave = false;
            Debug.LogError("Export failed : file name != ''");
            return;
        }

        if (paths.Count == 0)
        {
            isSave = false;
            Debug.LogError("Export failed : paths count != 0");
            return;
        }

        string fileName = FileName;
        string dataPath = Application.dataPath + "/_Game/TextAssets/" + fileName + ".txt";
        Debug.Log("Save to path : " + dataPath);

        System.IO.File.WriteAllText(dataPath, ""); // clear old file, if any

        for (int i = 0; i < paths.Count; i++)
        {
            Vector3 pos = paths[i];
            string line = "";
            if (i < paths.Count - 1)
            {
                line = System.String.Format("{0,3:f2};{1,3:f2};{2,3:f2}\r\n", pos.x, pos.y, pos.z);
            }
            else
            {
                line = System.String.Format("{0,3:f2};{1,3:f2};{2,3:f2}", pos.x, pos.y, pos.z);
            }
            System.IO.File.AppendAllText(dataPath, line);
        }
        isSave = false;
        Debug.Log("Save completed.");
    }

    public void SetCenter()
    {
        transform.SetParent(null);
        Vector3 total = Vector3.zero;
        foreach (var item in paths)
        {
            total += item;
        }
        centerPos = total / paths.Count;
        centerObj.position = centerPos;
        transform.SetParent(centerObj);
        centerObj.position = Vector3.zero;
    }

    Transform lastSphere;
    Transform preLastSphere;
    public void ReloadPipe(List<Vector3> paths)
    {
        this.paths = paths;
        isGenRing = false;

        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        for (int i = 1; i < paths.Count; i++)
        {
            GenerateSphere(paths[i], paths[i - 1]);
        }
        SetCenter();
    }


    public Vector3 lastDirectionForRay;

    public void ReloadPipe(TextAsset textAsset)
    {
        Refresh();
        string[] levelTextArray = textAsset.text.Split('\n');

        for (int i = 0; i < levelTextArray.Length; i++)
        {
            string[] textFixed = levelTextArray[i].Split(';');
            paths.Add(new Vector3(float.Parse(textFixed[0]), float.Parse(textFixed[1]), float.Parse(textFixed[2])));
        }

        for (int i = 1; i < paths.Count; i++)
        {
            GenerateSphere(paths[i], paths[i - 1]);
        }
        SetCenter();

        lastSphere = listSpheres[listSpheres.Count - 1].transform;
        preLastSphere = listSpheres[listSpheres.Count - 2].transform;
    }

    [NaughtyAttributes.Button]
    public void Refresh()
    {
        paths.Clear();
        listSpheres.Clear();
        isGenRing = false;

        foreach (Transform pipe in pipesStorage)
        {
            Destroy(pipe.gameObject);
        }

        foreach (BallRing ballRing in listBallRings)
        {
            ballRing.ReclaimBalls();
            Destroy(ballRing.gameObject);
        }

        foreach (GameObject ring in listRings)
        {
            Destroy(ring.gameObject);
        }
        listRings.Clear();
    }

}
