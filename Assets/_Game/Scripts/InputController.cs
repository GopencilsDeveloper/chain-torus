﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    private Vector3 currentTouchPos;
    private Vector3 lastTouchPos;
    private Vector3 directionTouch;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            currentTouchPos = lastTouchPos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            currentTouchPos = Input.mousePosition;
            directionTouch = currentTouchPos - lastTouchPos;
            lastTouchPos = currentTouchPos;
        }
        if (Input.GetMouseButtonUp(0))
        {
            directionTouch = Vector3.zero;
        }

        //if (Input.GetMouseButton(0))
        //{
        //    float h = speed * Input.GetAxis("Mouse X");
        //    float v = speed * Input.GetAxis("Mouse Y");
        //    print("X: " + Input.GetAxis("Mouse X") + " Y: " + Input.GetAxis("Mouse Y"));
        //    rigid.AddTorque(new Vector3(v, -h, 0f), ForceMode.VelocityChange);
        //}
    }
    #endregion
}
