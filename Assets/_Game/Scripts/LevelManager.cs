﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    #region Editor Params
    public List<TextAsset> levelTextAssets;
    public GeneratePipe generatePipe;
    public Transform center;
    #endregion

    #region Params
    public static LevelManager Instance { get; private set; }
    int index = -1;
    public TextAsset currentTextAsset;
    public int currentTorusOut;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            NextLV();
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            PreviousLV();
        }
    }

    public void SpawnLevel()
    {
        Refresh();
        int temp = DataManager.Instance.currentLevel;
        if (temp > levelTextAssets.Count - 1)
        {
            temp = (int)UnityEngine.Random.Range(0, levelTextAssets.Count);
        }
        currentTextAsset = levelTextAssets[temp];
        generatePipe.ReloadPipe(currentTextAsset);
    }

    void Refresh()
    {
        currentTorusOut = 0;
    }

    public void AddTorusOut()
    {
        currentTorusOut++;
        if (currentTorusOut == 3)
        {
            GameManager.Instance.WinGame();
        }
    }

    #endregion

    #region DEBUG

    public void NextLV()
    {
        index++;
        if (index > levelTextAssets.Count - 1)
        {
            index = 0;
        }
        currentTextAsset = levelTextAssets[index];
        generatePipe.ReloadPipe(currentTextAsset);
    }

    public void PreviousLV()
    {
        index--;
        if (index < 0)
        {
            index = levelTextAssets.Count - 1;
        }
        currentTextAsset = levelTextAssets[index];
        generatePipe.ReloadPipe(currentTextAsset);
    }

    #endregion
}
