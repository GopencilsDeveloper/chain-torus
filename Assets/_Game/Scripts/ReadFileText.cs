﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadFileText : MonoBehaviour
{
    public GeneratePipe generatePipe;
    public List<TextAsset> listTextAssets;
    public TextAsset textAsset;

    public List<Vector3> positions = new List<Vector3>();

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            if (textAsset == null) return;
            ReadLevelText(textAsset);
            ReloadPipe();
            generatePipe.SetCenter();
        }
    }

    public void ReadLevelText(TextAsset textAsset)
    {
        positions.Clear();
        string[] levelTextArray = textAsset.text.Split('\n');

        for (int i = 0; i < levelTextArray.Length; i++)
        {
            string[] textFixed = levelTextArray[i].Split(';');
            positions.Add(new Vector3(float.Parse(textFixed[0]), float.Parse(textFixed[1]), float.Parse(textFixed[2])));
        }
    }


    public void ReloadPipe()
    {
        generatePipe.ReloadPipe(positions);
    }

}

