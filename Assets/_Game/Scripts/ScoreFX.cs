﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreFX : MonoBehaviour
{
    public Animator animator;
    public TMPro.TextMeshPro tmpScore;

    public void SetScore(int score)
    {
        if (score == 1)
            tmpScore.fontSize = 25f;
        else
            tmpScore.fontSize = 40f;

        tmpScore.text = "+" + score.ToString();
    }

    [NaughtyAttributes.Button]
    public void DoAnim()
    {
        animator.SetTrigger("ScoreUp");
        Invoke("ReturnToPool", 0.5f);
    }

    public void ReturnToPool()
    {
        ScoreFXPool.Instance.ReturnToPool(this);
    }
}
