﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Theme
{
    public Color plumberColor;
    public Color torusColor;
    public Texture BGTexture;
}

public class ThemeManager : MonoBehaviour
{
    #region Editor Params
    [Header("THEMES")]
    public List<Theme> themes;

    [Header("ELEMENTS")]
    public Material plumberMaterial;
    public Material torusMaterial;
    public Material BGMaterial;
    public Material groundMaterial;
    #endregion

    #region Params
    public static ThemeManager Instance { get; private set; }
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        Instance = this;
    }

    [NaughtyAttributes.Button]
    public void RandomTheme()
    {
        Theme theme = themes[(int)Random.Range(0, themes.Count)];
        plumberMaterial.SetColor("_Color", theme.plumberColor);
        torusMaterial.SetColor("_Color", theme.torusColor);
        BGMaterial.SetTexture("_MainTex", theme.BGTexture);
        groundMaterial.SetTexture("_MainTex", theme.BGTexture);
    }

    #endregion
}
