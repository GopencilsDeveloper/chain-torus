﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torus : MonoBehaviour
{
    #region Editor Params
    public Rigidbody rgbd;
    public GameObject compoundColliders;
    public List<GameObject> listChild;
    #endregion

    #region Params
    private bool cache;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnEnable()
    {
        cache = false;
    }

    private void Update()
    {
        if (IsOut() && !cache)
        {
            LevelManager.Instance.AddTorusOut();
            transform.SetParent(transform.root);
            cache = true;
        }
    }

    public bool IsOut()
    {
        return transform.position.y <= -35f;
    }

    public void Active()
    {
        compoundColliders.SetActive(true);
        rgbd.isKinematic = false;
    }

    public void Deactive()
    {
        compoundColliders.SetActive(false);
        rgbd.isKinematic = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("TopHole"))
        {
            gameObject.layer = LayerMask.NameToLayer("OnHole");
            foreach (GameObject child in listChild)
            {
                child.layer = LayerMask.NameToLayer("OnHole");
            }
        }
    }

    #endregion
}
