﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Editor Params
    [Header("SCREENS")]
    public GameObject Menu;
    public GameObject InGame;
    public GameObject WinGame;
    public GameObject LoseGame;

    [Header("INGAME")]
    public Text txtLevelL;
    public Text txtLevelR;
    public Text txtScore;
    #endregion

    #region Params
    public static UIManager Instance { get; private set; }
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        RegisterEvents();
    }

    private void OnDisable()
    {
        UnregisterEvents();
    }

    private void RegisterEvents()
    {
        GameManager.OnStateChanged += OnGameStateChanged;
    }

    private void UnregisterEvents()
    {
        GameManager.OnStateChanged -= OnGameStateChanged;
    }

    public void OnGameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.MENU:
                OnMenu();
                break;
            case GameState.INGAME:
                OnInGame();
                break;
            case GameState.WINGAME:
                OnWinGame();
                break;
            case GameState.LOSEGAME:
                OnLoseGame();
                break;
        }
        UpdateLevel();
    }

    public void OnMenu()
    {
        Menu.SetActive(true);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
    }
    public void OnInGame()
    {
        Menu.SetActive(false);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
    }
    public void OnWinGame()
    {
        Menu.SetActive(false);
        WinGame.SetActive(true);
        LoseGame.SetActive(false);
    }
    public void OnLoseGame()
    {
        Menu.SetActive(false);
        WinGame.SetActive(false);
        LoseGame.SetActive(true);
    }

    public void UpdateLevel()
    {
        txtLevelL.text = (DataManager.Instance.currentLevel + 1).ToString();
        txtLevelR.text = (DataManager.Instance.currentLevel + 2).ToString();
    }

    public void UpdateScore(int addedScore)
    {
        DataManager.Instance.currentScore += addedScore;
        txtScore.text = DataManager.Instance.currentScore.ToString();
    }

    #endregion
}